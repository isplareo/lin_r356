﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="20008000">
	<Property Name="Instrument Driver" Type="Str">True</Property>
	<Property Name="NI.Lib.DefaultMenu" Type="Str">dir.mnu</Property>
	<Property Name="NI.Lib.Description" Type="Str">LabVIEW Plug and Play instrument driver for

&lt;fill in information about manufacturer, model, and type of the instrument&gt;.</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)W!!!*Q(C=\&gt;3^41."%)&lt;B$U2![J!!#&lt;G&amp;;=%NO)723%C&gt;/D1""5R!!W\B%AK9&amp;NQ!A6N9XNM&lt;%!BD*S#"R"V\NL`&gt;H8XOBZ0[&gt;CF&gt;K*X;HIZO&lt;`X5'Q_P8T\X4[(K?(D_5]X8E@F4$2XM@T5=70^,PYZO``X``8_\`]4'G08*14_]37&gt;&gt;3EN;U*RG`?R;EC&gt;ZEC&gt;ZEC&gt;ZE!&gt;ZE!&gt;ZE!?ZETOZETOZETOZE2OZE2OZE2NZX]F&amp;,H+2=V73R:/&amp;EK**A71Q&amp;#7(R*.Y%E`CY;=34_**0)EH]4"%C3@R**\%EXC9JM34?"*0YEE]F/K3\$MZHM2$?17?Q".Y!E`A95E&amp;HA!1,"95$IL!5.!:@!E]A3@Q]&amp;7"*`!%HM!4?/B7Y!E]A3@Q""[G^+M3840OZ(AI)]@D?"S0YX%]F*&lt;D=4S/R`%Y(J;4YX%]$M*:U#E/1=YE:Y$TQ`%Y(D\E?"S0YX%]DI?O@I?]8ZF2-_\E?!S0Y4%]BM@Q5%+'R`!9(M.D?#ALQW.Y$)`B-4QM*=.D?!S0!4%7:8E:R9S*RC!D-$T]^;@&amp;_FW+,L'_V\`G^+#K(E$6A[6[9&amp;10AOI'KW[=[I;I,L4K!KIOD/K%63?C!KI76B65$&gt;3?YYYWU,;U$7V&amp;7^)7N$FN.E\^ZI(\`6[\X5\$-'C\X7KTW7CV7GGZ8'KR7'A_HWMWG\W^L7\9J_XMQXPJ[K'VO['V[XM_HVO\@?TRGH%`V;&lt;XUG^Y._J=\&gt;VLHGPU!B=XD3U!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="Private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="StatusByte To Status.vi" Type="VI" URL="../Private/StatusByte To Status.vi"/>
		<Item Name="Default Instrument Setup.vi" Type="VI" URL="../Private/Default Instrument Setup.vi"/>
		<Item Name="CommRead.vi" Type="VI" URL="../Private/CommRead.vi"/>
		<Item Name="CommWrite.vi" Type="VI" URL="../Private/CommWrite.vi"/>
		<Item Name="CommQuery.vi" Type="VI" URL="../Private/CommQuery.vi"/>
		<Item Name="Status To Error.vi" Type="VI" URL="../Private/Status To Error.vi"/>
	</Item>
	<Item Name="Public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Action-Status" Type="Folder">
			<Item Name="Move to Absolute Position.vi" Type="VI" URL="../Public/Action-Status/Move to Absolute Position.vi"/>
			<Item Name="Move to Relative Position.vi" Type="VI" URL="../Public/Action-Status/Move to Relative Position.vi"/>
			<Item Name="SetPosition.vi" Type="VI" URL="../Public/Action-Status/SetPosition.vi"/>
			<Item Name="GetPosition.vi" Type="VI" URL="../Public/Action-Status/GetPosition.vi"/>
			<Item Name="SetVelocity.vi" Type="VI" URL="../Public/Action-Status/SetVelocity.vi"/>
			<Item Name="GetVelocity.vi" Type="VI" URL="../Public/Action-Status/GetVelocity.vi"/>
			<Item Name="SetAccel.vi" Type="VI" URL="../Public/Action-Status/SetAccel.vi"/>
			<Item Name="SetPower.vi" Type="VI" URL="../Public/Action-Status/SetPower.vi"/>
			<Item Name="Action-Status.mnu" Type="Document" URL="/&lt;instrlib&gt;/Lin Engineering R356/Public/Action-Status/Action-Status.mnu"/>
		</Item>
		<Item Name="Configure" Type="Folder">
			<Item Name="Configure.mnu" Type="Document" URL="/&lt;instrlib&gt;/Lin Engineering R356/Public/Configure/Configure.mnu"/>
			<Item Name="SetMovePolarity.vi" Type="VI" URL="../Public/Configure/SetMovePolarity.vi"/>
			<Item Name="SetSmoothness.vi" Type="VI" URL="../Public/Configure/SetSmoothness.vi"/>
			<Item Name="ClearBuffer.vi" Type="VI" URL="../Public/Configure/ClearBuffer.vi"/>
		</Item>
		<Item Name="Data" Type="Folder">
			<Item Name="Data.mnu" Type="Document" URL="/&lt;instrlib&gt;/Lin Engineering R356/Public/Data/Data.mnu"/>
		</Item>
		<Item Name="Utility" Type="Folder">
			<Item Name="Utility.mnu" Type="Document" URL="/&lt;instrlib&gt;/Lin Engineering R356/Public/Utility/Utility.mnu"/>
			<Item Name="Error Query.vi" Type="VI" URL="../Public/Utility/Error Query.vi"/>
			<Item Name="Reset.vi" Type="VI" URL="../Public/Utility/Reset.vi"/>
			<Item Name="Revision Query.vi" Type="VI" URL="../Public/Utility/Revision Query.vi"/>
			<Item Name="Self-Test.vi" Type="VI" URL="../Public/Utility/Self-Test.vi"/>
			<Item Name="Test Completion.vi" Type="VI" URL="../Public/Utility/Test Completion.vi"/>
			<Item Name="Wait For Completion.vi" Type="VI" URL="../Public/Utility/Wait For Completion.vi"/>
		</Item>
		<Item Name="dir.mnu" Type="Document" URL="/&lt;instrlib&gt;/Lin Engineering R356/Public/dir.mnu"/>
		<Item Name="Close.vi" Type="VI" URL="../Public/Close.vi"/>
		<Item Name="Initialize.vi" Type="VI" URL="../Public/Initialize.vi"/>
		<Item Name="VI Tree.vi" Type="VI" URL="../Public/VI Tree.vi"/>
		<Item Name="R356DriverNumber.ctl" Type="VI" URL="../Public/R356DriverNumber.ctl"/>
		<Item Name="Status.ctl" Type="VI" URL="../Public/Status.ctl"/>
	</Item>
	<Item Name="Lin Engineering R356 Readme.html" Type="Document" URL="/&lt;instrlib&gt;/Lin Engineering R356/Lin Engineering R356 Readme.html"/>
	<Item Name="R356Template.vit" Type="VI" URL="/&lt;instrlib&gt;/Lin Engineering R356/Private/R356Template.vit"/>
</Library>
